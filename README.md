# jg

Command line tool for the Jenkins G project

## Contributing guideline

After adding your code change you **must** increase the version manually in the ```jg``` file. To do so you have to change the following variable to contain the new version:

```
JG_VERSION="0.0.X"
```
> **_NOTE:_**  X is the current version of the variable + 1

After that, you can push your code to the remote repository and create the Pull Request. 

Once that pull request is merged into the ```master``` branch you should pull the new changes of that branch. After that, you should execute the following command to generate and push a new git tag in the repository.

```
./jg create-tag
```
> **_IMPORTANT:_** You have to execute the aforementioned command from the ```master``` branch once you've pulled the new changes.

Finally, if you need to use this new version you should update the ```builder-base``` Dockerfile to use the version you've generated.
